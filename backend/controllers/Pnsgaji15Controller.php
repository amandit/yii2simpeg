<?php

namespace backend\controllers;

use Yii;
use backend\models\Pnsgaji15;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;
/**
 * Pnsgaji15Controller implements the CRUD actions for Pnsgaji15 model.
 */
class Pnsgaji15Controller extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pnsgaji15 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Pnsgaji15::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pnsgaji15 model.
     * @param string $bulan
     * @param string $nip
     * @return mixed
     */
    public function actionView($bulan, $nip)
    {
        return $this->render('view', [
            'model' => $this->findModel($bulan, $nip),
        ]);
    }
    /**
     * Menampilkan Query Gaji Berdasarkan Nip dan Skpd.
     * Jika benar, akan menampilkan ke view/gridview.
     * @return dataprovider
     */
    public function actionList($id,$kdskpd)
{
        $nip=  $this->Nip($id);
        $sql="select A.*,B.nama,C.uraian as nmgol from pnsgaji15 A"
            . " inner join pnsmain B on A.nip=B.nip "
            . " inner join ref_pangkat C on A.kdgol=C.kdgol "
            . " where A.nip='$nip' and A.kdskpd='$kdskpd' and A.bulan='04'";
        
        $conn= Yii::$app->db;
        $cmd=$conn->createCommand($sql);
        $users=$cmd->queryAll();
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
   // $users = \app\models\User::find()->all();
    return $users;
}

public function GajiQuery($id,$kdskpd)
 {
 $nip=$id;
    
    $sql1="select COUNT(*) from pnsgaji15 A"
            . " inner join pnsmain B on A.nip=B.nip "
            . " inner join ref_pangkat C on A.kdgol=C.kdgol "
            . " where A.nip='$nip' and A.kdskpd='$kdskpd' and A.bulan='04'";
    
$sql="select A.*,B.nama,C.uraian as nmgol from pnsgaji15 A"
            . " inner join pnsmain B on A.nip=B.nip "
            . " inner join ref_pangkat C on A.kdgol=C.kdgol "
            . " where A.nip='$nip' and A.kdskpd='$kdskpd' and A.bulan='04'";
$count=  \Yii::$app->db->createCommand($sql1)->queryScalar();

$dataProvider=new SqlDataProvider(['sql'=>$sql, 
  //  'keyField' => $key,
    'totalCount'=>$count,
    'sort'=>[
      
        'attributes'=>[
             'uraian','nip','nama','kelahiran','tgllahir','kdgol','jnskel',
            'bulan', 'rp_pokok','rp_istri','rp_anak','rp_struk',
            'rp_fung','rp_umum','rp_tambah','rp_beras','rp_pajak','rp_bulat',
            'rp_kotor','rp_iwp','pot_tprm','pot_sewa','pot_lebih',
            'pot_lain2','rp_totpot','rp_bersih'
        ],
   ],
    'pagination'=>[
        'pageSize'=>100,
    ],
]);
 
/*$conn=  Yii::app()->db;
$cmd=$conn->createCommand($sql);
$dataProvider=$cmd->queryAll();
 return $dataProvider;
 * 
 */
return $dataProvider;
 }
 
    public function actionGaji($id,$kdskpd) {
      $sql="SELECT A.nip, A.nama,A.agama,A.kelahiran,A.tgllahir, A.jnskel, A.nipold, A.alamat,"
                  . " B.kdgol, B.status,B.user, B.kdskpd,C.uraian,C.tahun FROM pnsmain A "
                  . " INNER JOIN pnsgaji15 B ON (A.nip = B.nip) "
                  . " inner join ref_skpd C on (B.kdskpd=C.kdskpd)  WHERE C.tahun='2015' and length(C.kdskpd)>7 and B.bulan = '04' "
                  . " And B.nip =:id And B.kdskpd=:kdskpd "
                  . " ORDER BY B.kdgol DESC, B.rp_bersih DESC, A.nama ASC";
        $conn=\Yii::$app->db;
        $cmd=$conn->createCommand($sql);
        $cmd->bindParam(":id", $id);
        $cmd->bindParam(":kdskpd", $kdskpd);
        $model=$cmd->queryone();
       $dataProvider=  $this->GajiQuery($id,$kdskpd);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model'=>$model
        ]);
    }
    /**
     * Creates a new Pnsgaji15 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pnsgaji15();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'bulan' => $model->bulan, 'nip' => $model->nip]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pnsgaji15 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $bulan
     * @param string $nip
     * @return mixed
     */
    public function actionUpdate($bulan, $nip)
    {
        $model = $this->findModel($bulan, $nip);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'bulan' => $model->bulan, 'nip' => $model->nip]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pnsgaji15 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $bulan
     * @param string $nip
     * @return mixed
     */
    public function actionDelete($bulan, $nip)
    {
        $this->findModel($bulan, $nip)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pnsgaji15 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $bulan
     * @param string $nip
     * @return Pnsgaji15 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($bulan, $nip)
    {
        if (($model = Pnsgaji15::findOne(['bulan' => $bulan, 'nip' => $nip])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
