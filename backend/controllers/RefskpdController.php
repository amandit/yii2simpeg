<?php

namespace backend\controllers;

use Yii;
use backend\models\Refskpd;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;

/**
 * RefskpdController implements the CRUD actions for Refskpd model.
 */
class RefskpdController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Refskpd models.
     * @return mixed
     */
    public function actionIndex()
    {
        $kdskpd=Yii::$app->request->post('kdskpd');
       $sql="select kdskpd,uraian,tahun from ref_skpd where tahun=2014 "
               . " and CONCAT_WS('',uraian,kdskpd) LIKE '%".$kdskpd."%'"; 
        $dataProvider = new ActiveDataProvider([
            'query' =>  Refskpd::findBySql($sql),
            'pagination'=>[
                'pageSize'=>10
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Refskpd model.
     * @param string $tahun
     * @param string $kdskpd
     * @return mixed
     */
    public function actionView($tahun, $kdskpd)
    {
        return $this->render('view', [
            'model' => $this->findModel($tahun, $kdskpd),
        ]);
    }

    /**
     * Creates a new Refskpd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Refskpd();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'tahun' => $model->tahun, 'kdskpd' => $model->kdskpd]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Refskpd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $tahun
     * @param string $kdskpd
     * @return mixed
     */
    public function actionUpdate($tahun, $kdskpd)
    {
        $model = $this->findModel($tahun, $kdskpd);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'tahun' => $model->tahun, 'kdskpd' => $model->kdskpd]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Refskpd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $tahun
     * @param string $kdskpd
     * @return mixed
     */
    public function actionDelete($tahun, $kdskpd)
    {
        $this->findModel($tahun, $kdskpd)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Refskpd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $tahun
     * @param string $kdskpd
     * @return Refskpd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($tahun, $kdskpd)
    {
        if (($model = Refskpd::findOne(['tahun' => $tahun, 'kdskpd' => $kdskpd])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
