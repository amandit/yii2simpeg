<?php

namespace backend\controllers;

use Yii;
use mPDF;
use backend\models\Pnsmain;
use backend\models\Pnsgaji15;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
/**
 * PnsmainController implements the CRUD actions for Pnsmain model.
 */
class PnsmainController extends Controller
{
    public $skpd='';
    
    public function behaviors()
    {
    
        return [
                'access'=>[
            'class'=> AccessControl::className(),
            'rules'=>[
                [
                    'actions'=>['pnsmain','error','create','update'],
                    'allow'=>true,
                ],
                [
                    'actions'=>['logout','index','view','topdf'],
                    'allow'=>true,
                    'roles'=>['@'],
                ],
            ],
        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pnsmain models.
     * @return mixed
     */
    public function PnsQuery(){
     
    if (isset($_GET['kdskpd'])){
         $kdskpd=trim($_GET['kdskpd']);
         if (substr($kdskpd,0,1)=='9'){
             $lcFilter = " ";
          
         }else{
             $lcFilter = "AND LEFT(B.kdskpd,1) != '9' ";
        
        
             if (strlen($kdskpd)>7){
                 $lcFilter = $lcFilter . "AND B.kdskpd = '". $kdskpd ."'";
             }else{
           $lcFilter = $lcFilter . "AND left(B.kdskpd,7) = '". $kdskpd ."'";
                 
             }
              if (isset($_GET['nip'])){
               
               $nip="AND CONCAT_WS('',A.nama,B.nip) LIKE '%".trim($_GET['nip'])."%'";
             $lcFilter = $lcFilter . $nip;
            
         }    
      }
        }else{
             $lcFilter=" ";
         
        }
        
      
         $key="nip";
    $sql1="SELECT COUNT(*) FROM pnsmain A " 
                 . " INNER JOIN pnsgaji15 B ON (A.nip = B.nip) "
                  . " inner join ref_skpd C on (B.kdskpd=C.kdskpd)  WHERE C.tahun='2015' and length(C.kdskpd)>7 and B.bulan = '04' $lcFilter "
                  . " ORDER BY B.kdgol DESC, B.rp_bersih DESC, A.nama ASC";
    
$sql="SELECT A.nip, A.nama, B.kdgol, B.status, A.jnskel, B.user, A.nipold, B.kdskpd,C.uraian,C.tahun FROM pnsmain A "
                  . " INNER JOIN pnsgaji15 B ON (A.nip = B.nip) "
                  . " inner join ref_skpd C on (B.kdskpd=C.kdskpd)  WHERE C.tahun='2015' and length(C.kdskpd)>7 and B.bulan = '04' $lcFilter "
                  . " ORDER BY B.kdgol DESC, B.rp_bersih DESC, A.nama ASC";
$count=  \Yii::$app->db->createCommand($sql1)->queryScalar();

$dataProvider=new SqlDataProvider(['sql'=>$sql, 
  //  'keyField' => $key,
    'totalCount'=>$count,
    'sort'=>[
      
        'attributes'=>[
             'uraian','nip','nama','kelahiran','tgllahir','kdgol','jnskel',
            'bulan', 'rp_pokok','rp_istri','rp_anak','rp_struk',
            'rp_fung','rp_umum','rp_tambah','rp_beras','rp_pajak',
            'rp_kotor','rp_iwp','pot_tprm','pot_sewa','pot_lebih',
            'pot_lain2','rp_totpot','rp_bersih'
        ],
   ],
    'pagination'=>[
        'pageSize'=>100,
    ],
]);
return $dataProvider;
    }
    public function actionIndex()
    {
     
        
        /* $dataProvider = new ActiveDataProvider([
            'query' => Pnsmain::find(),
        ]);
        * 
        */
        $dataProvider=  $this->PnsQuery();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
/**
 *  View Single Report
 * @param string $nip
 * 
 */
    
    public function actionTopdf($nip){
          $sql="select A.nip,B.nama,A.kdgol,C.uraian from pnsmain B
inner join pnsgaji15 A on B.nip=A.nip
inner join ref_pangkat C on A.kdgol=C.kdgol
where A.bulan='05' and left(A.kdskpd,1)!='9' and A.nip=:nip";
    $conn=  Yii::$app->db;
    $cmd=$conn->createCommand($sql);
    $cmd->bindParam(':nip', $nip);
    $data=$cmd->queryOne();
    
        $mpdf = new mPDF();
        $mpdf->writeHTML(
                $this->renderPartial('print',array(
			'data' => $data,
                        
		),TRUE)
                );
        $mpdf->Output();
        exit();        
    }

    /**
     * Displays a single Pnsmain model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id,$kdskpd)
    {
        
        $sql="SELECT A.nip, A.nama,A.agama,A.kelahiran,A.tgllahir, A.jnskel, A.nipold, A.alamat,"
                  . " B.kdgol, B.status,B.user, B.kdskpd,C.uraian,C.tahun FROM pnsmain A "
                  . " INNER JOIN pnsgaji15 B ON (A.nip = B.nip) "
                  . " inner join ref_skpd C on (B.kdskpd=C.kdskpd)  WHERE C.tahun='2015' and length(C.kdskpd)>7 and B.bulan = '04' "
                  . " And B.nip =:id And B.kdskpd=:kdskpd "
                  . " ORDER BY B.kdgol DESC, B.rp_bersih DESC, A.nama ASC";
        $conn=\Yii::$app->db;
        $cmd=$conn->createCommand($sql);
        $cmd->bindParam(":id", $id);
        $cmd->bindParam(":kdskpd", $kdskpd);
        $model=$cmd->queryOne();
        return $this->render('view', [
            'model' => $model,//$this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pnsmain model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pnsmain();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 
                'id' => $model->nip,
                ]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pnsmain model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($nip,$kdskpd)
    {
//        $modelx = New Pnsmain();
 
        $model = $this->findModel($nip);
       
        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
    //          $model->imageFile->saveAs('uploads/'.$nameimg.'.'.$model->imageFile->extension);
      if ($model->upload()){   
            $model->save();
             Yii::$app->session->setFlash('success', 'Update Sukses');
            return $this->redirect(['view', 
                'id' => $model->nip,
                  'kdskpd'=>  $kdskpd
]);
        } }
            return $this->render('update', [
                'model' => $model,
            ]);
        
    }

    /**
     * Deletes an existing Pnsmain model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($nip)
    {
        $this->findModel($nip)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pnsmain model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Pnsmain the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nip)
    {
        if (($model = Pnsmain::findOne($nip)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
