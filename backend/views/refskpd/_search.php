<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\RefskpdSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="refskpd-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tahun') ?>

    <?= $form->field($model, 'kdskpd') ?>

    <?= $form->field($model, 'uraian') ?>

    <?= $form->field($model, 'pim_jab1') ?>

    <?= $form->field($model, 'pim_jab2') ?>

    <?php // echo $form->field($model, 'pim_nm') ?>

    <?php // echo $form->field($model, 'pim_nip') ?>

    <?php // echo $form->field($model, 'bend_jab') ?>

    <?php // echo $form->field($model, 'bend_nm') ?>

    <?php // echo $form->field($model, 'bend_nip') ?>

    <?php // echo $form->field($model, 'oprt_jab') ?>

    <?php // echo $form->field($model, 'oprt_nm') ?>

    <?php // echo $form->field($model, 'oprt_nip') ?>

    <?php // echo $form->field($model, 'alamat') ?>

    <?php // echo $form->field($model, 'kota') ?>

    <?php // echo $form->field($model, 'telp') ?>

    <?php // echo $form->field($model, 'cetak') ?>

    <?php // echo $form->field($model, 'nonpwp') ?>

    <?php // echo $form->field($model, 'rekbank') ?>

    <?php // echo $form->field($model, 'pdbank') ?>

    <?php // echo $form->field($model, 'user') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
