<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Refskpd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="refskpd-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kdskpd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uraian')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pim_jab1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pim_jab2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pim_nm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pim_nip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bend_jab')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bend_nm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bend_nip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oprt_jab')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oprt_nm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oprt_nip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kota')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cetak')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nonpwp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rekbank')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pdbank')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
