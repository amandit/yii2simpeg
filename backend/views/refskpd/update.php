<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Refskpd */

$this->title = 'Update Refskpd: ' . ' ' . $model->tahun;
$this->params['breadcrumbs'][] = ['label' => 'Refskpds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tahun, 'url' => ['view', 'tahun' => $model->tahun, 'kdskpd' => $model->kdskpd]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="refskpd-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
