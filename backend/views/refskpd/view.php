<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Refskpd */

$this->title = $model->tahun;
$this->params['breadcrumbs'][] = ['label' => 'Refskpds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="refskpd-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'tahun' => $model->tahun, 'kdskpd' => $model->kdskpd], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'tahun' => $model->tahun, 'kdskpd' => $model->kdskpd], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Apakah anda yakin data ini mau di hapus?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tahun',
            'kdskpd',
            'uraian',
            'pim_jab1',
            'pim_jab2',
            'pim_nm',
            'pim_nip',
            'bend_jab',
            'bend_nm',
            'bend_nip',
            'oprt_jab',
            'oprt_nm',
            'oprt_nip',
            'alamat',
            'kota',
            'telp',
            'cetak',
            'nonpwp',
            'rekbank',
            'pdbank',
            'user',
        ],
    ]) ?>

</div>
