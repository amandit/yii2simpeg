<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar SKPD';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="refskpd-index">
  <?php Pjax::begin(); ?>
<?= Html::beginForm(['index'], 'post', ['data-pjax' => '', 'class' => 'form-inline']); ?>
      <div class="row">
        <div class="col-md-12">
   
    <?= Html::input('text', 'kdskpd', Yii::$app->request->post('kdskpd'), ['class' => 'form-control','style'=>'width:1000px']) ?>
        </div>
    </div>   
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-flat ']) ?>
      <?= Html::a('Reset', ['index'], ['class' => 'btn btn-info btn-flat ' ]) ?>
            </div>
        </div>
    </div>
<?= Html::endForm() ?>
    
      <br>
    <p>
        <?= Html::a('Create Refskpd', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
         'pjax'=>true,
            'export'=>false,
        'toolbar'=> [
            '{export}',
        ],
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'kdskpd', 'format' => 'text', 'label' => 'Kode','headerOptions'=>['width'=>'10px']],
                ['attribute' => 'uraian', 'format' => 'text', 'label' => 'Nama SKPD','headerOptions'=>['width'=>'1000px']],
//                 'nama:text:Nama',
            
      
            ['class' => 'yii\grid\ActionColumn',
                 'contentOptions'=>['style'=>'width: 100px;'],
                   'template'=>'{update}{delete}',
            'buttons'=>[
             
                  'update'=>function($url,$model){
                    return Html::a('<span class="glyphicon glyphicon-edit"></span>',$url,[
                       'class'=>'btn btn-block btn-primary btn-flat',
               //         'style'=>'width : 90px',
                       /* 'data'=>[
                            'confirm'=>'Apakah Anda Yakin Ingin Hapus Item ini ?',
                            'method'=>'post',
                        ]
                        * 
                        */
                    ]);
                },
                
                'delete'=>function($url,$model){
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>',$url,[
                       'class'=>'btn btn-block btn-danger btn-flat',
                //        'style'=>'width : 90px',
                        'data'=>[
                            'confirm'=>'Apakah Anda Yakin Ingin Hapus Item ini ?',
                            'method'=>'post',
                        ]
                    ]);
                }
            ]
                ],
         
        ],
    ]); ?>
    
<?php Pjax::end(); ?>
</div>
