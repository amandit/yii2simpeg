<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Pnsgaji15 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pnsgaji15-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'bulan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kdskpd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kdgol')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'masathn')->textInput() ?>

    <?= $form->field($model, 'masabln')->textInput() ?>

    <?= $form->field($model, 'kdstruk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kdfung')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kdumum')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'stspns')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kawin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'stsgaji')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tenaga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'anak')->textInput() ?>

    <?= $form->field($model, 'rp_pokok')->textInput() ?>

    <?= $form->field($model, 'rp_istri')->textInput() ?>

    <?= $form->field($model, 'rp_anak')->textInput() ?>

    <?= $form->field($model, 'rp_struk')->textInput() ?>

    <?= $form->field($model, 'rp_fung')->textInput() ?>

    <?= $form->field($model, 'rp_umum')->textInput() ?>

    <?= $form->field($model, 'rp_tambah')->textInput() ?>

    <?= $form->field($model, 'rp_bulat')->textInput() ?>

    <?= $form->field($model, 'rp_beras')->textInput() ?>

    <?= $form->field($model, 'rp_pajak')->textInput() ?>

    <?= $form->field($model, 'rp_kotor')->textInput() ?>

    <?= $form->field($model, 'pot_tprm')->textInput() ?>

    <?= $form->field($model, 'pot_sewa')->textInput() ?>

    <?= $form->field($model, 'pot_lebih')->textInput() ?>

    <?= $form->field($model, 'pot_lain2')->textInput() ?>

    <?= $form->field($model, 'rp_iwp')->textInput() ?>

    <?= $form->field($model, 'rp_totpot')->textInput() ?>

    <?= $form->field($model, 'rp_bersih')->textInput() ?>

    <?= $form->field($model, 'tmt_cpns')->textInput() ?>

    <?= $form->field($model, 'tmt_pns')->textInput() ?>

    <?= $form->field($model, 'tmt_gol')->textInput() ?>

    <?= $form->field($model, 'tmt_masa')->textInput() ?>

    <?= $form->field($model, 'tmt_struk')->textInput() ?>

    <?= $form->field($model, 'tmt_fung')->textInput() ?>

    <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'berubah')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
