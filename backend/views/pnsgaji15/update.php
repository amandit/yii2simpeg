<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Pnsgaji15 */

$this->title = 'Update Pnsgaji15: ' . ' ' . $model->bulan;
$this->params['breadcrumbs'][] = ['label' => 'Pnsgaji15s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->bulan, 'url' => ['view', 'bulan' => $model->bulan, 'nip' => $model->nip]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pnsgaji15-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
