<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Pnsgaji15 */

$this->title = 'Create Pnsgaji15';
$this->params['breadcrumbs'][] = ['label' => 'Pnsgaji15s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pnsgaji15-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
