<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Pnsgaji15 */

$this->title = $model->bulan;
$this->params['breadcrumbs'][] = ['label' => 'Pnsgaji15s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pnsgaji15-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'bulan' => $model->bulan, 'nip' => $model->nip], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'bulan' => $model->bulan, 'nip' => $model->nip], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'bulan',
            'nip',
            'kdskpd',
            'kdgol',
            'masathn',
            'masabln',
            'kdstruk',
            'kdfung',
            'kdumum',
            'stspns',
            'kawin',
            'status',
            'stsgaji',
            'tenaga',
            'anak',
            'rp_pokok',
            'rp_istri',
            'rp_anak',
            'rp_struk',
            'rp_fung',
            'rp_umum',
            'rp_tambah',
            'rp_bulat',
            'rp_beras',
            'rp_pajak',
            'rp_kotor',
            'pot_tprm',
            'pot_sewa',
            'pot_lebih',
            'pot_lain2',
            'rp_iwp',
            'rp_totpot',
            'rp_bersih',
            'tmt_cpns',
            'tmt_pns',
            'tmt_gol',
            'tmt_masa',
            'tmt_struk',
            'tmt_fung',
            'keterangan',
            'berubah',
            'user',
        ],
    ]) ?>

</div>
