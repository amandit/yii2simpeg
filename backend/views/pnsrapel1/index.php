<?php

use yii\helpers\Html;
use yii\grid\GridView;

use backend\models\Pnsgaji15;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\Pnsrapel1Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detail Rapel';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pnsrapel1-index">

    <h1><?= Html::encode($this->title) ?></h1>
   <?php
use yii\helpers\Url;
?>
<div class="panel-body">
                      <table id="example" class="table table-hover table-bordered">
                    <tr>
                    <td>SKPD</td>
                    <td><?= $model['kdskpd'].'-'.$model['uraian']; ?></td>
                        <td rowspan="9"><div class="pull-right image">
                         <?php 
                          if (!empty($model['nip'])){
                              $imgnip=Yii::$app->MyComponent->nip($model['nip']);
                        $img= $imgnip.".jpg";
                            }else {
     $img="";
 
                            }
                         ?>
                            <img src="<?= Yii::$app->request->BaseUrl. '/uploads/'.$img; ?>" class="img-rounded" height="300" width="250" alt="User Image" style="border: 3px solid #333333;" />
                        </div></td>
                    </tr>
                    <tr>
                    <td width="250">No Induk</td>
                    <td width="550"><?= $model['nip']; ?></td>
                    </tr>
                    <tr>
                    <td>Nama</td>
                    <td><?= $model['nama']; ?></td>
                    </tr>
                    <tr>
                    <td>Jenis Kelamin</td>
                    <td><?= $model['jnskel']; ?></td>
                    </tr>
                    <tr>
                    <td>agama</td>
                    <td><?= $model['agama']; ?></td>
                    </tr>
                    <tr>
                    <td>Tempat dan Tanggal Lahir</td>
                    <td><?= $model['kelahiran'].'-'.$model['tgllahir']; ?></td>
                    </tr>
                    <tr>
                    <td>Alamat</td>
                    <td><?= $model['alamat']; ?></td>
                    </tr>
                   </table>
                  
                <div class="text-right">
                    <a href="<?= Url::to(['pnsmain/index']) ?>" class="btn btn-sm btn-warning"> Kembali <i class="fa fa-arrow-circle-right"></i></a>
                </div>  
                                </div>
    <div style="overflow: auto;">
        
  
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
               [ 'attribute'=>'bulan',
                'format'=>'text',
                'value'=> function ($data){
                return Pnsgaji15::bulan($data['bulan']);}],
                'rp_pokok:integer:Gaji Pokok',
                'rp_istri:integer:Tunj. Istri',
                'rp_anak:integer:Tunj. Anak',
                'rp_struk:integer:Tunj. Struktural',
                'rp_fung:integer:Tunj. Fungsional',
                'rp_umum:integer:Tunj. Umum',
                'rp_tambah:integer:Tambahan Tunj.',
                'rp_beras:integer:Tunj. Beras',
                'rp_pajak:integer:Tunj. Pajak PPh',
                'rp_bulat:integer:Pembulatan',
                'rp_kotor:integer:Penghasilan',
                'rp_iwp:integer:Potongan IWP',
                'rp_pajak:integer:Pajak PPh 21',
                'pot_tprm:integer:Pot. Teperum',
                'pot_sewa:integer:Sewa Rumah',
                'pot_lebih:integer:Lebih Bayar',
                'pot_lain2:integer:Pot. Lain-Lain',
                'rp_totpot:integer:Potongan',
                'rp_bersih:integer:Gaji Bersih',
          
            

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 </div>
</div>
