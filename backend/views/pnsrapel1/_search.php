<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Pnsrapel1Search */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pnsrapel1-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'thnbln') ?>

    <?= $form->field($model, 'nip') ?>

    <?= $form->field($model, 'urut') ?>

    <?= $form->field($model, 'kdskpd') ?>

    <?= $form->field($model, 'thnbln1') ?>

    <?php // echo $form->field($model, 'thnbln2') ?>

    <?php // echo $form->field($model, 'thnbln3') ?>

    <?php // echo $form->field($model, 'tanggal') ?>

    <?php // echo $form->field($model, 'jenis') ?>

    <?php // echo $form->field($model, 'rp_pokok') ?>

    <?php // echo $form->field($model, 'rp_istri') ?>

    <?php // echo $form->field($model, 'rp_anak') ?>

    <?php // echo $form->field($model, 'rp_struk') ?>

    <?php // echo $form->field($model, 'rp_fung') ?>

    <?php // echo $form->field($model, 'rp_umum') ?>

    <?php // echo $form->field($model, 'rp_tambah') ?>

    <?php // echo $form->field($model, 'rp_bulat') ?>

    <?php // echo $form->field($model, 'rp_beras') ?>

    <?php // echo $form->field($model, 'rp_pajak') ?>

    <?php // echo $form->field($model, 'rp_kotor') ?>

    <?php // echo $form->field($model, 'rp_iwp') ?>

    <?php // echo $form->field($model, 'pot_tprm') ?>

    <?php // echo $form->field($model, 'pot_lain') ?>

    <?php // echo $form->field($model, 'rp_totpot') ?>

    <?php // echo $form->field($model, 'rp_bersih') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'user') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
