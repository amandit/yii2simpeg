<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Pnsrapel1 */

$this->title = 'Update Pnsrapel1: ' . ' ' . $model->thnbln;
$this->params['breadcrumbs'][] = ['label' => 'Pnsrapel1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->thnbln, 'url' => ['view', 'thnbln' => $model->thnbln, 'nip' => $model->nip, 'urut' => $model->urut]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pnsrapel1-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
