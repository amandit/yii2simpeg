<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Pnsrapel1 */

$this->title = $model->thnbln;
$this->params['breadcrumbs'][] = ['label' => 'Pnsrapel1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pnsrapel1-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'thnbln' => $model->thnbln, 'nip' => $model->nip, 'urut' => $model->urut], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'thnbln' => $model->thnbln, 'nip' => $model->nip, 'urut' => $model->urut], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'thnbln',
            'nip',
            'urut',
            'kdskpd',
            'thnbln1',
            'thnbln2',
            'thnbln3',
            'tanggal',
            'jenis',
            'rp_pokok',
            'rp_istri',
            'rp_anak',
            'rp_struk',
            'rp_fung',
            'rp_umum',
            'rp_tambah',
            'rp_bulat',
            'rp_beras',
            'rp_pajak',
            'rp_kotor',
            'rp_iwp',
            'pot_tprm',
            'pot_lain',
            'rp_totpot',
            'rp_bersih',
            'keterangan',
            'user',
        ],
    ]) ?>

</div>
