<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Pnsrapel1 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pnsrapel1-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'thnbln')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'urut')->textInput() ?>

    <?= $form->field($model, 'kdskpd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'thnbln1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'thnbln2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'thnbln3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggal')->textInput() ?>

    <?= $form->field($model, 'jenis')->textInput() ?>

    <?= $form->field($model, 'rp_pokok')->textInput() ?>

    <?= $form->field($model, 'rp_istri')->textInput() ?>

    <?= $form->field($model, 'rp_anak')->textInput() ?>

    <?= $form->field($model, 'rp_struk')->textInput() ?>

    <?= $form->field($model, 'rp_fung')->textInput() ?>

    <?= $form->field($model, 'rp_umum')->textInput() ?>

    <?= $form->field($model, 'rp_tambah')->textInput() ?>

    <?= $form->field($model, 'rp_bulat')->textInput() ?>

    <?= $form->field($model, 'rp_beras')->textInput() ?>

    <?= $form->field($model, 'rp_pajak')->textInput() ?>

    <?= $form->field($model, 'rp_kotor')->textInput() ?>

    <?= $form->field($model, 'rp_iwp')->textInput() ?>

    <?= $form->field($model, 'pot_tprm')->textInput() ?>

    <?= $form->field($model, 'pot_lain')->textInput() ?>

    <?= $form->field($model, 'rp_totpot')->textInput() ?>

    <?= $form->field($model, 'rp_bersih')->textInput() ?>

    <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
