<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Pnsmain */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pnsmain-form">

    <?php $form = ActiveForm::begin([
	    'options' => [ 'enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'nip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kelahiran')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tgllahir')->textInput() ?>

    <?= $form->field($model, 'jnskel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agama')->textInput(['maxlength' => true]) ?>

     <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>
       <?= $form->field($model,'imageFile' )->fileInput() ?>
     
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
