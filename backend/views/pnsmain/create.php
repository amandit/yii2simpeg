<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Pnsmain */

$this->title = 'Create Pnsmain';
$this->params['breadcrumbs'][] = ['label' => 'Pnsmains', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pnsmain-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
