<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PnsmainSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pnsmain-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nip') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'kelahiran') ?>

    <?= $form->field($model, 'tgllahir') ?>

    <?= $form->field($model, 'jnskel') ?>

    <?php // echo $form->field($model, 'agama') ?>

    <?php // echo $form->field($model, 'rekbank') ?>

    <?php // echo $form->field($model, 'npwp') ?>

    <?php // echo $form->field($model, 'sekolah') ?>

    <?php // echo $form->field($model, 'alamat') ?>

    <?php // echo $form->field($model, 'karpeg') ?>

    <?php // echo $form->field($model, 'nipold') ?>

    <?php // echo $form->field($model, 'btspens') ?>

    <?php // echo $form->field($model, 'stssertf') ?>

    <?php // echo $form->field($model, 'kdsertf') ?>

    <?php // echo $form->field($model, 'stsout') ?>

    <?php // echo $form->field($model, 'tglout') ?>

    <?php // echo $form->field($model, 'ketout') ?>

    <?php // echo $form->field($model, 'blnout') ?>

    <?php // echo $form->field($model, 'user') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
