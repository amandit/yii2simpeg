<?php
use yii\helpers\Url;
use yii\helpers\Html;

?>
<div class="panel-body">
                      <table id="example" class="table table-hover table-bordered">
                    <tr>
                    <td>SKPD</td>
                    <td><?= 
$model['kdskpd'].'-'.$model['uraian']; ?></td>
                    <td rowspan="9"><div class="pull-right image">
                         <?php 
                          if (!empty($model['nip'])){
                              $imgnip=Yii::$app->MyComponent->nip($model['nip']);
                        $img= $imgnip.".jpg";
                            }else {
     $img="";
 
                            }
                         ?>
                            <img src="<?= Yii::$app->request->BaseUrl. '/uploads/'.$img; ?>" class="img-rounded" height="300" width="250" alt="User Image" style="border: 3px solid #333333;" />
                        </div></td>
                    </tr>
                    <tr>
                    <td width="250">No Induk</td>
                    <td width="550"><?= $model['nip']; ?></td>
                    </tr>
                    <tr>
                    <td>Nama</td>
                    <td><?= $model['nama']; ?></td>
                    </tr>
                    <tr>
                    <td>Jenis Kelamin</td>
                    <td><?= $model['jnskel']; ?></td>
                    </tr>
                    <tr>
                    <td>agama</td>
                    <td><?= $model['agama']; ?></td>
                    </tr>
                    <tr>
                    <td>Tempat dan Tanggal Lahir</td>
                    <td><?= $model['kelahiran'].'-'.$model['tgllahir']; ?></td>
                    </tr>
                    <tr>
                    <td>Alamat</td>
                    <td><?= $model['alamat']; ?></td>
                    </tr>
                   </table>
                 
     
    <div class="row">
        <div class="col-md-10">
               <?= Html::a('Update <i class="fa  fa-edit "></i>', ['update?nip='.$model['nip'].'&kdskpd='.$model['kdskpd']], ['class' => 'btn btn-flat btn-sm btn-info ' ]) ?>
                        
               <?= Html::a('Print <i class="fa  fa-print "></i>', ['topdf?nip='.$model['nip']], ['class' => 'btn btn-sm btn-flat btn-success' ]) ?>
                        
        </div>
        <div class="col-md-2">
                    <a href="<?= Url::to('index') ?>" class="btn btn-flat btn-sm btn-warning"> Kembali <i class="fa fa-arrow-circle-right"></i></a>
            
        </div>
                </div>  
                                </div>