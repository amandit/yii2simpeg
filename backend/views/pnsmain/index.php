<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\pajax;
use backend\models\Pnsmain;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Button;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detail PNS';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pnsmain-index">

    <h1><?= "Pencarian" ?></h1>
    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
    ]);
    ?>
    <div class="row">
        <div class="col-md-12">
            <?=
            Select2::widget([
                'id' => 'kdskpd',
                'name' => 'kdskpd',
                'value' => '',
                'data' => Pnsmain::getSkpd(),
                'options' => ['multiple' => false, 'placeholder' => 'Pilih SKPD ...']
            ]);
            ?>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-12">
            <input type="text" class="form-control" name="nip" id="nip"></input>
        </div>
    </div>    <br>
    <div class="row">
        <div class="col-sm-5">
            <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Reset', ['index'], ['class' => 'btn btn-info']) ?>
            </div>
        </div>
    </div>
        <?php ActiveForm::end(); ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'pjax'=>true,
            'export'=>false,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'uraian', 'format' => 'text', 'label' => 'Nama SKPD','headerOptions'=>['width'=>'100']],
                ['attribute' => 'nip', 'format' => 'text', 'label' => 'Nip','headerOptions'=>['width'=>'175']],
                 'nama:text:Nama',
                ['attribute' => 'kdgol', 'format' => 'text', 'label' => 'Gol.','headerOptions'=>['width'=>'40']],
              
               
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {gaji} {rapel}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-th-list"> View</span>', $url, [
                                      ' class'=>"btn btn-default btn-flat",
                                'title' => Yii::t('yii', 'View'),
                                            'style'=>'width : 90px',
                            ]);
                        },
                                'gaji' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-bitcoin"> Gaji</span>', $url, [
                                 'class'=>"btn btn-danger btn-flat",
                                        'title' => Yii::t('yii', 'Gaji'),
                                 'style'=>'width : 90px;'
                            ]);
                        },
                                'rapel' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-rub"> Rapel</span>', $url, [
                                        'class'=>"btn btn-info btn-flat",
                                'title' => Yii::t('yii', 'Rapel'),
                                'style'=>'width : 90px'
                            ]);
                        },
                            ],
                            'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url = Url::to(['view', 'id' => $model["nip"], 'kdskpd' => $model["kdskpd"]]);
                            return $url;
                        }
                              if ($action === 'gaji') {
                            $url = Url::to(['pnsgaji15/gaji', 'id' => $model["nip"], 'kdskpd' => $model["kdskpd"]]);
                            return $url;
                        }
                               if ($action === 'rapel') {
                            $url = Url::to(['pnsrapel1/rapel', 'id' => $model["nip"], 'kdskpd' => $model["kdskpd"]]);
                            return $url;
                        }
                    }
                    
                        ]],
                ]);
                ?>

    </div>
