<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Pnsmain */

$this->title = $model->nip;
$this->params['breadcrumbs'][] = ['label' => 'Pnsmains', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pnsmain-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->nip], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->nip], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nip',
            'nama',
            'kelahiran',
            'tgllahir',
            'jnskel',
            'agama',
            'rekbank',
            'npwp',
            'sekolah',
            'alamat:ntext',
            'karpeg',
            'nipold',
            'btspens',
            'stssertf',
            'kdsertf',
            'stsout',
            'tglout',
            'ketout:ntext',
            'blnout',
            'user',
        ],
    ]) ?>

</div>
