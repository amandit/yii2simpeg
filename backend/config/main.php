<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
         'controllerMap' => [
        'file' => 'mdm\\upload\\FileController', // use to show or download file
    ],
    'modules' => [
        'gridview' =>  [
        'class' => '\kartik\grid\Module'
         ],
    ],
    'components' => [
        'session' => [
'class' => 'yii\web\DbSession',
// Set the following if you want to use DB component other than
// default 'db'.
// 'db' => 'mydb',
 
// To override default session table, set the following
// 'sessionTable' => 'my_session',
],
        'urlManager' => [
		'enablePrettyUrl' => true,
		'showScriptName' => false,
		],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        //My Component : Isinya Berupa Function Public
         'MyComponent'=>[
            'class'=>'backend\components\MyComponent',
        ],
    
    ],
    'params' => $params,

];
