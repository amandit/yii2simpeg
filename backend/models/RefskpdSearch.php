<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Refskpd;

/**
 * RefskpdSearch represents the model behind the search form about `backend\models\Refskpd`.
 */
class RefskpdSearch extends Refskpd
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tahun', 'kdskpd', 'uraian', 'pim_jab1', 'pim_jab2', 'pim_nm', 'pim_nip', 'bend_jab', 'bend_nm', 'bend_nip', 'oprt_jab', 'oprt_nm', 'oprt_nip', 'alamat', 'kota', 'telp', 'cetak', 'nonpwp', 'rekbank', 'pdbank', 'user'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Refskpd::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'kdskpd', $this->kdskpd])
            ->andFilterWhere(['like', 'uraian', $this->uraian])
            ->andFilterWhere(['like', 'pim_jab1', $this->pim_jab1])
            ->andFilterWhere(['like', 'pim_jab2', $this->pim_jab2])
            ->andFilterWhere(['like', 'pim_nm', $this->pim_nm])
            ->andFilterWhere(['like', 'pim_nip', $this->pim_nip])
            ->andFilterWhere(['like', 'bend_jab', $this->bend_jab])
            ->andFilterWhere(['like', 'bend_nm', $this->bend_nm])
            ->andFilterWhere(['like', 'bend_nip', $this->bend_nip])
            ->andFilterWhere(['like', 'oprt_jab', $this->oprt_jab])
            ->andFilterWhere(['like', 'oprt_nm', $this->oprt_nm])
            ->andFilterWhere(['like', 'oprt_nip', $this->oprt_nip])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'kota', $this->kota])
            ->andFilterWhere(['like', 'telp', $this->telp])
            ->andFilterWhere(['like', 'cetak', $this->cetak])
            ->andFilterWhere(['like', 'nonpwp', $this->nonpwp])
            ->andFilterWhere(['like', 'rekbank', $this->rekbank])
            ->andFilterWhere(['like', 'pdbank', $this->pdbank])
            ->andFilterWhere(['like', 'user', $this->user]);

        return $dataProvider;
    }
}
