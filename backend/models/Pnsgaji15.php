<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pnsgaji15".
 *
 * @property string $bulan
 * @property string $nip
 * @property string $kdskpd
 * @property string $kdgol
 * @property integer $masathn
 * @property integer $masabln
 * @property string $kdstruk
 * @property string $kdfung
 * @property string $kdumum
 * @property string $stspns
 * @property string $kawin
 * @property string $status
 * @property string $stsgaji
 * @property string $tenaga
 * @property integer $anak
 * @property integer $rp_pokok
 * @property integer $rp_istri
 * @property integer $rp_anak
 * @property integer $rp_struk
 * @property integer $rp_fung
 * @property integer $rp_umum
 * @property integer $rp_tambah
 * @property integer $rp_bulat
 * @property integer $rp_beras
 * @property integer $rp_pajak
 * @property integer $rp_kotor
 * @property integer $pot_tprm
 * @property integer $pot_sewa
 * @property integer $pot_lebih
 * @property integer $pot_lain2
 * @property integer $rp_iwp
 * @property integer $rp_totpot
 * @property integer $rp_bersih
 * @property string $tmt_cpns
 * @property string $tmt_pns
 * @property string $tmt_gol
 * @property string $tmt_masa
 * @property string $tmt_struk
 * @property string $tmt_fung
 * @property string $keterangan
 * @property string $berubah
 * @property string $user
 */
class Pnsgaji15 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pnsgaji15';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bulan', 'nip', 'kdskpd', 'kdgol', 'masathn', 'masabln', 'kdstruk', 'kdfung', 'kdumum', 'kawin', 'status', 'stsgaji', 'tenaga', 'anak', 'rp_pokok', 'rp_istri', 'rp_anak', 'rp_struk', 'rp_fung', 'rp_umum', 'rp_tambah', 'rp_bulat', 'rp_beras', 'rp_pajak', 'rp_kotor', 'pot_tprm', 'pot_sewa', 'pot_lebih', 'pot_lain2', 'rp_iwp', 'rp_totpot', 'rp_bersih', 'keterangan', 'user'], 'required'],
            [['masathn', 'masabln', 'anak', 'rp_pokok', 'rp_istri', 'rp_anak', 'rp_struk', 'rp_fung', 'rp_umum', 'rp_tambah', 'rp_bulat', 'rp_beras', 'rp_pajak', 'rp_kotor', 'pot_tprm', 'pot_sewa', 'pot_lebih', 'pot_lain2', 'rp_iwp', 'rp_totpot', 'rp_bersih'], 'integer'],
            [['kdumum', 'stspns', 'berubah'], 'string'],
            [['tmt_cpns', 'tmt_pns', 'tmt_gol', 'tmt_masa', 'tmt_struk', 'tmt_fung'], 'safe'],
            [['bulan'], 'string', 'max' => 4],
            [['nip'], 'string', 'max' => 21],
            [['kdskpd'], 'string', 'max' => 11],
            [['kdgol', 'kdstruk', 'kdfung'], 'string', 'max' => 5],
            [['kawin', 'tenaga'], 'string', 'max' => 2],
            [['status'], 'string', 'max' => 6],
            [['stsgaji'], 'string', 'max' => 1],
            [['keterangan'], 'string', 'max' => 75],
            [['user'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bulan' => 'Bulan',
            'nip' => 'Nip',
            'kdskpd' => 'Kdskpd',
            'kdgol' => 'Kdgol',
            'masathn' => 'Masathn',
            'masabln' => 'Masabln',
            'kdstruk' => 'Kdstruk',
            'kdfung' => 'Kdfung',
            'kdumum' => 'Kdumum',
            'stspns' => 'Stspns',
            'kawin' => 'Kawin',
            'status' => 'Status',
            'stsgaji' => 'Stsgaji',
            'tenaga' => 'Tenaga',
            'anak' => 'Anak',
            'rp_pokok' => 'Rp Pokok',
            'rp_istri' => 'Rp Istri',
            'rp_anak' => 'Rp Anak',
            'rp_struk' => 'Rp Struk',
            'rp_fung' => 'Rp Fung',
            'rp_umum' => 'Rp Umum',
            'rp_tambah' => 'Rp Tambah',
            'rp_bulat' => 'Rp Bulat',
            'rp_beras' => 'Rp Beras',
            'rp_pajak' => 'Rp Pajak',
            'rp_kotor' => 'Rp Kotor',
            'pot_tprm' => 'Pot Tprm',
            'pot_sewa' => 'Pot Sewa',
            'pot_lebih' => 'Pot Lebih',
            'pot_lain2' => 'Pot Lain2',
            'rp_iwp' => 'Rp Iwp',
            'rp_totpot' => 'Rp Totpot',
            'rp_bersih' => 'Rp Bersih',
            'tmt_cpns' => 'Tmt Cpns',
            'tmt_pns' => 'Tmt Pns',
            'tmt_gol' => 'Tmt Gol',
            'tmt_masa' => 'Tmt Masa',
            'tmt_struk' => 'Tmt Struk',
            'tmt_fung' => 'Tmt Fung',
            'keterangan' => 'Keterangan',
            'berubah' => 'Berubah',
            'user' => 'User',
        ];
    }
          public static function iif($kondisi, $true, $false) {
return ($kondisi ? $true : $false);
}

        public static function bulan($bln) {
$cBln = Pnsgaji15::iif(gettype($bln)=="string",(int) $bln,$bln);
$nama_bln = array(1 => "Januari", "Februari", "Maret", "April", "Mei","Juni", "Juli", "Agustus", "September","Oktober", "November", "Desember");
return $nama_bln[$cBln];
}
}
