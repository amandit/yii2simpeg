<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Pnsrapel1;

/**
 * Pnsrapel1Search represents the model behind the search form about `backend\models\Pnsrapel1`.
 */
class Pnsrapel1Search extends Pnsrapel1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['thnbln', 'nip', 'kdskpd', 'thnbln1', 'thnbln2', 'thnbln3', 'tanggal', 'keterangan', 'user'], 'safe'],
            [['urut', 'jenis', 'rp_pokok', 'rp_istri', 'rp_anak', 'rp_struk', 'rp_fung', 'rp_umum', 'rp_tambah', 'rp_bulat', 'rp_beras', 'rp_pajak', 'rp_kotor', 'rp_iwp', 'pot_tprm', 'pot_lain', 'rp_totpot', 'rp_bersih'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pnsrapel1::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'urut' => $this->urut,
            'tanggal' => $this->tanggal,
            'jenis' => $this->jenis,
            'rp_pokok' => $this->rp_pokok,
            'rp_istri' => $this->rp_istri,
            'rp_anak' => $this->rp_anak,
            'rp_struk' => $this->rp_struk,
            'rp_fung' => $this->rp_fung,
            'rp_umum' => $this->rp_umum,
            'rp_tambah' => $this->rp_tambah,
            'rp_bulat' => $this->rp_bulat,
            'rp_beras' => $this->rp_beras,
            'rp_pajak' => $this->rp_pajak,
            'rp_kotor' => $this->rp_kotor,
            'rp_iwp' => $this->rp_iwp,
            'pot_tprm' => $this->pot_tprm,
            'pot_lain' => $this->pot_lain,
            'rp_totpot' => $this->rp_totpot,
            'rp_bersih' => $this->rp_bersih,
        ]);

        $query->andFilterWhere(['like', 'thnbln', $this->thnbln])
            ->andFilterWhere(['like', 'nip', $this->nip])
            ->andFilterWhere(['like', 'kdskpd', $this->kdskpd])
            ->andFilterWhere(['like', 'thnbln1', $this->thnbln1])
            ->andFilterWhere(['like', 'thnbln2', $this->thnbln2])
            ->andFilterWhere(['like', 'thnbln3', $this->thnbln3])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'user', $this->user]);

        return $dataProvider;
    }
}
