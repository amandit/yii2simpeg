<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Pnsmain;

/**
 * PnsmainSearch represents the model behind the search form about `backend\models\Pnsmain`.
 */
class PnsmainSearch extends Pnsmain
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nip', 'nama', 'kelahiran', 'tgllahir', 'jnskel', 'agama', 'rekbank', 'npwp', 'sekolah', 'alamat', 'karpeg', 'nipold', 'btspens', 'stssertf', 'kdsertf', 'stsout', 'tglout', 'ketout', 'blnout', 'user'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pnsmain::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tgllahir' => $this->tgllahir,
            'btspens' => $this->btspens,
            'tglout' => $this->tglout,
        ]);

        $query->andFilterWhere(['like', 'nip', $this->nip])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'kelahiran', $this->kelahiran])
            ->andFilterWhere(['like', 'jnskel', $this->jnskel])
            ->andFilterWhere(['like', 'agama', $this->agama])
            ->andFilterWhere(['like', 'rekbank', $this->rekbank])
            ->andFilterWhere(['like', 'npwp', $this->npwp])
            ->andFilterWhere(['like', 'sekolah', $this->sekolah])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'karpeg', $this->karpeg])
            ->andFilterWhere(['like', 'nipold', $this->nipold])
            ->andFilterWhere(['like', 'stssertf', $this->stssertf])
            ->andFilterWhere(['like', 'kdsertf', $this->kdsertf])
            ->andFilterWhere(['like', 'stsout', $this->stsout])
            ->andFilterWhere(['like', 'ketout', $this->ketout])
            ->andFilterWhere(['like', 'blnout', $this->blnout])
            ->andFilterWhere(['like', 'user', $this->user]);

        return $dataProvider;
    }
}
