<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pnsrapel1".
 *
 * @property string $thnbln
 * @property string $nip
 * @property integer $urut
 * @property string $kdskpd
 * @property string $thnbln1
 * @property string $thnbln2
 * @property string $thnbln3
 * @property string $tanggal
 * @property integer $jenis
 * @property integer $rp_pokok
 * @property integer $rp_istri
 * @property integer $rp_anak
 * @property integer $rp_struk
 * @property integer $rp_fung
 * @property integer $rp_umum
 * @property integer $rp_tambah
 * @property integer $rp_bulat
 * @property integer $rp_beras
 * @property integer $rp_pajak
 * @property integer $rp_kotor
 * @property integer $rp_iwp
 * @property integer $pot_tprm
 * @property integer $pot_lain
 * @property integer $rp_totpot
 * @property integer $rp_bersih
 * @property string $keterangan
 * @property string $user
 */
class Pnsrapel1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pnsrapel1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['thnbln', 'nip', 'urut', 'kdskpd', 'thnbln3', 'jenis'], 'required'],
            [['urut', 'jenis', 'rp_pokok', 'rp_istri', 'rp_anak', 'rp_struk', 'rp_fung', 'rp_umum', 'rp_tambah', 'rp_bulat', 'rp_beras', 'rp_pajak', 'rp_kotor', 'rp_iwp', 'pot_tprm', 'pot_lain', 'rp_totpot', 'rp_bersih'], 'integer'],
            [['tanggal'], 'safe'],
            [['thnbln', 'thnbln1', 'thnbln2'], 'string', 'max' => 6],
            [['nip'], 'string', 'max' => 21],
            [['kdskpd'], 'string', 'max' => 11],
            [['thnbln3'], 'string', 'max' => 7],
            [['keterangan'], 'string', 'max' => 75],
            [['user'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'thnbln' => 'Thnbln',
            'nip' => 'Nip',
            'urut' => 'Urut',
            'kdskpd' => 'Kdskpd',
            'thnbln1' => 'Thnbln1',
            'thnbln2' => 'Thnbln2',
            'thnbln3' => 'Thnbln3',
            'tanggal' => 'Tanggal',
            'jenis' => 'Jenis',
            'rp_pokok' => 'Rp Pokok',
            'rp_istri' => 'Rp Istri',
            'rp_anak' => 'Rp Anak',
            'rp_struk' => 'Rp Struk',
            'rp_fung' => 'Rp Fung',
            'rp_umum' => 'Rp Umum',
            'rp_tambah' => 'Rp Tambah',
            'rp_bulat' => 'Rp Bulat',
            'rp_beras' => 'Rp Beras',
            'rp_pajak' => 'Rp Pajak',
            'rp_kotor' => 'Rp Kotor',
            'rp_iwp' => 'Rp Iwp',
            'pot_tprm' => 'Pot Tprm',
            'pot_lain' => 'Pot Lain',
            'rp_totpot' => 'Rp Totpot',
            'rp_bersih' => 'Rp Bersih',
            'keterangan' => 'Keterangan',
            'user' => 'User',
        ];
    }
}
