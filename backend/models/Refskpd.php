<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ref_skpd".
 *
 * @property string $tahun
 * @property string $kdskpd
 * @property string $uraian
 * @property string $pim_jab1
 * @property string $pim_jab2
 * @property string $pim_nm
 * @property string $pim_nip
 * @property string $bend_jab
 * @property string $bend_nm
 * @property string $bend_nip
 * @property string $oprt_jab
 * @property string $oprt_nm
 * @property string $oprt_nip
 * @property string $alamat
 * @property string $kota
 * @property string $telp
 * @property string $cetak
 * @property string $nonpwp
 * @property string $rekbank
 * @property string $pdbank
 * @property string $user
 */
class Refskpd extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_skpd';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tahun', 'kdskpd', 'pim_nip', 'bend_nip', 'oprt_nip', 'alamat', 'kota'], 'required'],
            [['cetak'], 'string'],
            [['tahun'], 'string', 'max' => 4],
            [['kdskpd'], 'string', 'max' => 11],
            [['uraian', 'alamat'], 'string', 'max' => 100],
            [['pim_jab1', 'pim_jab2', 'bend_jab', 'oprt_jab', 'kota'], 'string', 'max' => 50],
            [['pim_nm', 'bend_nm', 'oprt_nm'], 'string', 'max' => 40],
            [['pim_nip', 'bend_nip', 'oprt_nip'], 'string', 'max' => 22],
            [['telp', 'nonpwp', 'rekbank', 'user'], 'string', 'max' => 20],
            [['pdbank'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tahun' => 'Tahun',
            'kdskpd' => 'Kode',
            'uraian' => 'Nama SKPD',
            'pim_jab1' => 'Pim Jab1',
            'pim_jab2' => 'Pim Jab2',
            'pim_nm' => 'Pim Nm',
            'pim_nip' => 'Pim Nip',
            'bend_jab' => 'Bend Jab',
            'bend_nm' => 'Bend Nm',
            'bend_nip' => 'Bend Nip',
            'oprt_jab' => 'Oprt Jab',
            'oprt_nm' => 'Oprt Nm',
            'oprt_nip' => 'Oprt Nip',
            'alamat' => 'Alamat',
            'kota' => 'Kota',
            'telp' => 'Telp',
            'cetak' => 'Cetak',
            'nonpwp' => 'Nonpwp',
            'rekbank' => 'Rekbank',
            'pdbank' => 'Pdbank',
            'user' => 'User',
        ];
    }
}
