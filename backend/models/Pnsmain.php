<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "pnsmain".
 *
 * @property string $nip
 * @property string $nama
 * @property string $kelahiran
 * @property string $tgllahir
 * @property string $jnskel
 * @property string $agama
 * @property string $rekbank
 * @property string $npwp
 * @property string $sekolah
 * @property string $alamat
 * @property string $karpeg
 * @property string $nipold
 * @property string $btspens
 * @property string $stssertf
 * @property string $kdsertf
 * @property string $stsout
 * @property string $tglout
 * @property string $ketout
 * @property string $blnout
 * @property string $user
 * 
 */
class Pnsmain extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     *  @var UploadedFile
     */
    
   public $imageFile;
    public static function tableName()
    {
        return 'pnsmain';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nip', 'nipold','imageFile'], 'required'],
            [['tgllahir', 'btspens', 'tglout'], 'safe'],
            [['jnskel', 'alamat', 'stssertf', 'ketout'], 'string'],
            [['nip', 'nipold'], 'string', 'max' => 21],
            [['nama'], 'string', 'max' => 40],
            [['kelahiran', 'sekolah'], 'string', 'max' => 30],
            [['agama', 'stsout'], 'string', 'max' => 1],
            [['rekbank'], 'string', 'max' => 25],
              [['imageFile'], 'file', ],
            [['npwp', 'kdsertf', 'user'], 'string', 'max' => 20],
            [['karpeg'], 'string', 'max' => 7],
            [['blnout'], 'string', 'max' => 4],
        ];
    }

    public function upload()
    {
         $nameimg= Yii::$app->MyComponent->nip($this->nip);
        if ($this->validate()){
            $this->imageFile->saveAs('uploads/'.$nameimg.'.'.$this->imageFile->extension);
            return true;
        }else{
            return false;
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nip' => 'Nip',
            'nama' => 'Nama',
            'kelahiran' => 'Kelahiran',
            'tgllahir' => 'Tgllahir',
            'jnskel' => 'Jnskel',
            'agama' => 'Agama',
            'rekbank' => 'Rekbank',
            'npwp' => 'Npwp',
            'sekolah' => 'Sekolah',
            'alamat' => 'Alamat',
            'karpeg' => 'Karpeg',
            'nipold' => 'Nipold',
            'btspens' => 'Btspens',
            'stssertf' => 'Stssertf',
            'kdsertf' => 'Kdsertf',
            'stsout' => 'Stsout',
            'tglout' => 'Tglout',
            'ketout' => 'Ketout',
            'blnout' => 'Blnout',
            'user' => 'User',
            'kdgol'=>'Gol.',
            'uraian'=>'Nama SKPD',
            'imageFile'=> 'Upload Foto'
        ];
    }
    
    public static function Nip($nipx){
        $lnawal = substr($nipx, 0, 8);
        $lntengah = substr($nipx, 8, 6);
        $lntengahx = substr($nipx, 14, 1);
        $lnakhir = substr($nipx, -3, 3);
       $nipnew=$lnawal . ' ' . $lntengah . ' ' . $lntengahx . ' ' . $lnakhir;
        return $nipnew;
    }
       public static function getSkpd()
    {
        $droptions=  Refskpd::findBySql("Select kdskpd,uraian from ref_skpd where tahun='2015' and length(kdskpd)>7")->all();
        return ArrayHelper::map($droptions, 'kdskpd', 'uraian');
    }

}
